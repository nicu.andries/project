package com.example;

import com.example.algorithm.Calculator;
import com.example.util.Helper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {
    private Calculator calculator;
    private Helper helper;

    @Before
    public void beforeTest() {
        calculator = new Calculator();
        helper = new Helper();
    }

    @Test
    public void prepareExpression() {
        assertEquals(helper.prepareExpression("-(5+3 -2)"), "0-(5+3 -2)");
        assertEquals(helper.prepareExpression("-(-2)"), "0-(0-2)");
        assertEquals(helper.prepareExpression("2-5-(3-4)"), "2-5-(3-4)");
    }

    @Test
    public void calculate() {
        assertEquals(calculator.calculate("-(5+3 -2)"), -6);
        assertEquals(calculator.calculate("-(-2)"), 2);
        assertEquals(calculator.calculate("2-5-(3-4)"), -2);
    }
}
