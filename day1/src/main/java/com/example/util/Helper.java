package com.example.util;

public class Helper {

    public String prepareExpression(String expression) {
        StringBuilder preparedExpression = new StringBuilder();
        for (int i = 0; i < expression.length(); i++) {
            char symbol = expression.charAt(i);
            if (symbol == '-') {
                if (i == 0)
                    preparedExpression.append('0');
                else if (expression.charAt(i - 1) == '(')
                    preparedExpression.append('0');
            }
            preparedExpression.append(symbol);
        }
        return preparedExpression.toString();
    }
}
