package com.example.algorithm;

import com.example.util.Helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Calculator {
    private final Stack<Integer> numberStack = new Stack<>();
    private final Stack<Character> operatorStack = new Stack<>();
    private final Map<Character, Integer> operatorPrecedence = new HashMap<>();
    private final Helper helper;

    public Calculator() {
        operatorPrecedence.put('+', 1);
        operatorPrecedence.put('-', 1);
        operatorPrecedence.put('*', 2);
        operatorPrecedence.put('/', 2);
        helper = new Helper();
    }

    public int calculate(String inputExpression) {
        String expression = helper.prepareExpression(inputExpression);
        for (int i = 0; i < expression.length(); i++) {
            char character = expression.charAt(i);
            if (character == ' ') {
                continue;
            }
            if (character == '(') {
                operatorStack.push(character);
            } else if (character == ')') {
                while (operatorStack.peek() != '(') {
                    doNextOperation();
                }
                operatorStack.pop();
            } else if (Character.isDigit(character)) {
                int number = character - '0';
                while (++i < expression.length()) {
                    character = expression.charAt(i);
                    if (!Character.isDigit(character)) {
                        break;
                    }
                    number = number * 10 + (character - '0');
                }
                numberStack.push(number);
                i--;
            } else {
                if (!operatorStack.isEmpty() && compare(operatorStack.peek(), character) >= 0) {
                    doNextOperation();
                }
                operatorStack.push(character);
            }
        }
        while (doNextOperation()) ;
        return numberStack.pop();
    }

    private int compare(char firstOperator, char secondOperator) {
        if (firstOperator == '(') {
            return -1;
        }
        int firstPrecedence = operatorPrecedence.get(firstOperator);
        int secondPrecedence = operatorPrecedence.get(secondOperator);
        return Integer.compare(firstPrecedence, secondPrecedence);
    }

    private boolean doNextOperation() {
        if (numberStack.size() < 2) {
            return false;
        }
        int secondNumber = numberStack.pop();
        int firstNumber = numberStack.pop();
        if (operatorStack.isEmpty()) {
            return false;
        }
        char operator = operatorStack.pop();
        if (operator == '+')
            numberStack.push(firstNumber + secondNumber);
        else if (operator == '-')
            numberStack.push(firstNumber - secondNumber);
        else if (operator == '*')
            numberStack.push(firstNumber * secondNumber);
        else if (operator == '/')
            numberStack.push(firstNumber / secondNumber);
//        switch (operator) {
//            case '+' -> numberStack.push(firstNumber + secondNumber);
//            case '-' -> numberStack.push(firstNumber - secondNumber);
//            case '*' -> numberStack.push(firstNumber * secondNumber);
//            case '/' -> numberStack.push(firstNumber / secondNumber);
//        }
        return true;
    }
}